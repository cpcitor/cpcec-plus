
release: 
	gcc -DSDL2 -O2 -xc Source/cpcec.c -lSDL2 -lncurses -oBinary/cpcec-plus

debug: 
	gcc -DSDL2 -g -O0 -xc Source/cpcec.c -lSDL2 -lncurses -oBinary/cpcec-plus-debug
