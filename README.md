# CPCEC-PLUS

## Requirements
This special version of **CPCEC-20201215** has only been tested on **Linux** (to this day, no plans for Windows or MacOS).

## How to build
To compile, there are 2 targets:

- `make` to generate an optimized build. Output will be `./Binary/cpcec-plus`
- `make debug` to generate a build suitable for debugging (has symbols + no optimization). Output will be `./Binary/cpcec-plus-debug`

`SDL2` and `ncurses` development library are required to be installed on your system. On Debian-based Linux, those libraries can be installed using this command: `sudo apt-get install libsdl2-dev libncurses5-dev`.

## How to use / new features
### Differences with CPCEC
Use **cpcec-plus** as you would use **cpcec** (same command-line options, etc). cpcec-plus only provide additions on top of CPCEC.

### Symbols

#### RAM/ROM Labels

**cpcec-plus** brings symbols support to **cpcec**. Disassembled code will print the corresponding labels (when available).
- To generate symbols suitable for cpcec-plus, use RASM (https://github.com/EdouardBERGE/rasm) with `-sz` option. This will generate a `.sym` file next to the generated Z80 binary file.
- Add the option `-s:<absolute path to your symbol file>` while launching **cpcec-plus** to set the symbol file to use. As a reference, my personal command-line to launch **cpcec-plus** (as an Amstrad GX-4000 console) looks like this:

#### ROM Labels

**cpcec-plus** also brings support for "rom labels". When available, the debuggers prints the ROM's label next to the rom number. The file format is similar to a symbol file:

```
0: BigTileset0
1: BigTileset2
2: Tileset0_X1Y0
3: Tileset0_X0Y1
..
31: LastRom
```

- Add the option `-y:<absolute path to your rom labels file>` while launching **cpcec-plus** to set the roms label file to use.

#### Example

As a reference, my personal command-line to launch **cpcec-plus** (as an Amstrad GX-4000 console) looks like this:

```
cpcec-plus -k0 -m3 -g3 -c0 -O game.cpr -s:/home/norecess464/Development/GitLab/SonicGX/game.sym -y:/home/norecess464/Development/GitLab/SonicGX/game.rom_labels
```

### Debugger

When the command-line window has the focus, user can type one of the following keys to access the features:

```
While in emulation mode
	Q to quit
	ESC or Space to pause emulation and enter debugger

When in debugger:
	Q to quit
	F5 or ESC to get back to emulation
	F9 or B to toggle breakpoint
	F10 or Space for 'debug step into'
	T to reset NOPS counter
	D to set new disassembly address
	S to toggle Sprite Viewer
	P to toggle Palette Viewer
	M to toggle Monitor View (then Up/Down/PgUp/PgDown to navigate in dumped memory)
	W to run Z80 until specified scanline is reached
	E to export memory fragment as a binary file
	Up/Down/PgUp/PgDown to navigate in disassembled code
```

When the user interface is too small to print everything on screen, the H key may be used to switch pages 1-2.

## Credits
- CPCEC emulator by cngsoft - http://cngsoft.no-ip.org/cpcec.htm
- "plus" addon (ncurses-based debugger) by norecess/condense - http://norecess.cpcscene.net/

## Screenshots
![001](Screenshots/overall.png)

### Old versions
![002](Screenshots/running.png)
![003](Screenshots/scanline_100.png)
![004](Screenshots/palette.png)
![005](Screenshots/sprites.png)
