
#pragma once

#define COLORPAIR_RED		1
#define COLORPAIR_GREEN		2
#define COLORPAIR_YELLOW	3
#define COLORPAIR_BLUE		4
#define COLORPAIR_MAGENTA 	5
#define COLORPAIR_CYAN		6
#define COLORPAIR_WHITE		7

void InitTerminalColors()
{
	start_color();
	use_default_colors();

	init_pair(COLORPAIR_RED, COLOR_RED, -1);
	init_pair(COLORPAIR_GREEN, COLOR_GREEN, -1);
	init_pair(COLORPAIR_YELLOW, COLOR_YELLOW, -1);
	init_pair(COLORPAIR_BLUE, COLOR_BLUE, -1);
	init_pair(COLORPAIR_MAGENTA, COLOR_MAGENTA, -1);
	init_pair(COLORPAIR_CYAN, COLOR_CYAN, -1);
	init_pair(COLORPAIR_WHITE, COLOR_WHITE, -1);
}

void PushRed()
{
	attron(COLOR_PAIR(COLORPAIR_RED));
}
void PopRed()
{
	attroff(COLOR_PAIR(COLORPAIR_RED));
}

void PushGreen()
{
	attron(COLOR_PAIR(COLORPAIR_GREEN));
}
void PopGreen()
{
	attroff(COLOR_PAIR(COLORPAIR_GREEN));
}

void PushYellow()
{
	attron(COLOR_PAIR(COLORPAIR_YELLOW));
}
void PopYellow()
{
	attroff(COLOR_PAIR(COLORPAIR_YELLOW));
}

void PushBlue()
{
	attron(COLOR_PAIR(COLORPAIR_BLUE));
}
void PopBlue()
{
	attroff(COLOR_PAIR(COLORPAIR_BLUE));
}

void PushMagenta()
{
	attron(COLOR_PAIR(COLORPAIR_MAGENTA));
}
void PopMagenta()
{
	attroff(COLOR_PAIR(COLORPAIR_MAGENTA));
}

void PushCyan()
{
	attron(COLOR_PAIR(COLORPAIR_CYAN));
}
void PopCyan()
{
	attroff(COLOR_PAIR(COLORPAIR_CYAN));
}

void PushWhite()
{
	attron(COLOR_PAIR(COLORPAIR_WHITE));
}
void PopWhite()
{
	attroff(COLOR_PAIR(COLORPAIR_WHITE));
}

void EnablePen0()
{
	PushCyan();
}
void DisablePen0()
{
	PopCyan();
}
void EnablePen1()
{
	PushYellow();
}
void DisablePen1()
{
	PopYellow();
}
void EnablePen2()
{
	PushGreen();
}
void DisablePen2()
{
	PopGreen();
}
void EnablePen3()
{
	PushRed();
}
void DisablePen3()
{
	PopRed();
}
#define E0 EnablePen0() // labels
#define D0 DisablePen0()
#define E1 EnablePen1() // values
#define D1 DisablePen1()
#define E2 EnablePen2() // details
#define D2 DisablePen2()
#define E3 EnablePen3() // red
#define D3 DisablePen3()
