
#pragma once

void AskNewDisAddress()
{
	E0;mvprintw(WindowHeight-1, COLUMN_WIDTH, "Address? #");D0;

	DisAddress = AskHexadecimalAddress(false, 0);

	z80_debug_pnl0_w = DisAddress;
	ForceNextRefresh = true;
}

void AskWaitScanline()
{
	E0;mvprintw(WindowHeight-1, COLUMN_WIDTH, "Scanline? ");D0;

	char str[4]; // 3 chars + null terminator
	AskString(str, sizeof(str));
	unsigned short scanline = (unsigned short)strtol(str, NULL, 10);
	if (scanline < 312)
	{
		do
		{
			z80_main(1),z80_debug_reset();
		} while ((video_pos_y>>1) != scanline);

		// make sure to reach new HCC
		while (crtc_count_r0 > 30)
		{
			z80_main(1),z80_debug_reset();
		};
	}
	ForceNextRefresh = true;
}

void PrintZ80Disassembly(int StartX, int StartY, int LineCount)
{
	int StartXAddress = StartX;

	if (LabelSingleLine)
	{
		StartXAddress = 2;
	}

	static char tmpstr[256];

	static char opcode[32];
	static char byteDump[16];

	char offset, labelSize;
	WORD pc;
	int ui_y = StartY;
	int ui_max_y = StartY + LineCount;

	int y,x;
	WORD m,w;

	int opcodeCount = 0;
	m=z80_debug_pnl0_w;
	for (y=0;y<LineCount;++y)
	{
		bool isFirst = (opcodeCount == 0);

		if (opcodeCount < sizeof(DebugCache)/sizeof(WORD))
		{
			DebugCache[ opcodeCount++ ] = m;
		}

		x=z80_breakpoints[m];
		w=m-z80_pc.w;
		pc = m;
		m=z80_dasm(session_tmpstr,m);

		// replace all $ by #
		for (int i=0; i<sizeof(session_tmpstr); i++)
		{
			if (session_tmpstr[i]=='$')
			{
				session_tmpstr[i] = '#';
			}
		}
		
		// replace all ',' by ', '
		for (int i=0; i<sizeof(session_tmpstr); i++)
		{
			if (session_tmpstr[i]==',')
			{
				for (int j=sizeof(session_tmpstr)-1; j>=i+1; j--)
				{
					session_tmpstr[j] = session_tmpstr[j-1];
				}
			
				session_tmpstr[i+1] = ' ';
			}
		}

		// LABEL (AT LEFT)
		char *labelCurrentLine = GetLabel(pc);
		if (labelCurrentLine == NULL)
		{
			// skip one line if isFirst + label at first line
			if (isFirst && LabelSingleLine)
			{
				ui_y++;
				if (ui_y == ui_max_y)
				{
					return;
				}
			}
		}
		else
		{
			if (LabelSingleLine)
			{
				E0; mvprintw(ui_y, 0, "%s:\n", labelCurrentLine); D0;
				ui_y++;
				if (ui_y == ui_max_y)
				{
					return;
				}
			}
			else
			{
				int labelSize = strlen(labelCurrentLine);

				int xLabel = StartXAddress - labelSize - 3;

				if (xLabel < 0)
				{
					labelCurrentLine += -xLabel;
					xLabel = 0;
				}

				E0; mvprintw(ui_y, xLabel, labelCurrentLine); printw(":"); D0;
			}
		}

		// BREAKPOINT
		if (x)
		{
			if (x&8)
			{
			}
			else
			{
				E3; 
				mvaddch(ui_y, StartXAddress-2, 'B');
				mvaddch(ui_y, StartXAddress-1, '*');
				D3;
			}
		}

		// > ADDRESS <
		if (!w)
		{
			E3;
			mvprintw(ui_y, StartXAddress, "#%04X", pc);
			mvaddch(ui_y, StartXAddress+5, '>');
			D3;
		}
		else
		{
			// ADDRESS
			E1;mvprintw(ui_y, StartXAddress, "#%04X", pc);D1;
		}

		// LD A, (MY_LABEL)
		// format: "0123: DD364567 LD   (IX+$45),$67"
		char *opcode = &session_tmpstr[15];
		char *delim = strchr(opcode, '^');
		if (delim == NULL) // has label?
		{
			// no label
			bool isNumber = false;
			int size = strlen(opcode);
			int x = StartXAddress+7;			
			for (int i=0; i<size; i++)
			{
				char c = opcode[i];
				int col = 0;
				if ((c == '(') || (c == ')'))
				{
					col = 1;
				}

				if (c == '#')
				{
					col = 2;
					isNumber = true;				
				}
				else if (isNumber && (
						((c >= 'A') && (c <= 'F'))
					|| 	((c >= '0') && (c <= '9')) 
					))
				{
					col = 2;
				}
				else
				{
					isNumber = false;
				}
				
				switch(col)
				{
					case 2:
						E1;
						break;
					case 1:
						E1;
						break;
					case 0:
					default:
						E1;
						break;
				}
				
				mvaddch(ui_y, x++, c);
				
				switch(col)
				{
					case 2:
						D1;
						break;
					case 1:
						D1;
						break;
					case 0:
					default:
						D1;
						break;
				}
			}
		}
		else
		{
			move(ui_y, StartXAddress+7);
			while(opcode!=delim)
			{
				char c = *opcode;
				opcode++;
				
				if ((c == '(') || (c == ')'))
				{
					E1;
					addch(c);
					D1;
				}
				else
				{
					E1;
					addch(c);
					D1;
				}
			}
			
			opcode = delim+1;
			char *delim = strchr(opcode, '^');
			E1;
			while(opcode!=delim)
			{
				addch(*opcode);
				opcode++;
			}
			D1;

			opcode++;
			while (*opcode!=0)
			{
				char c = *opcode;
				opcode++;
				
				if ((c == '(') || (c == ')'))
				{
					E1;
					addch(c);
					D1;
				}
				else
				{
					E1;
					addch(c);
					D1;
				}
			}
		}
		
		if (ShowDetails)
		{
			E2;
			
			// VALUE DESCRIPTION. cf. decimal conversion			
			if (ValueDescription[0] != 0)
			{
				printw("%s", ValueDescription);
			}
			
			// ASIC LABEL
			if (OperandValue != 0)
			{
				char *asicLabelCurrentLine = GetAsicLabel(OperandValue);
				if (OpcodesColumn != -1) // is the column hidden? (because UI is too small)
				{
					if (asicLabelCurrentLine != NULL)
					{
						int asicLabelSize = strlen(asicLabelCurrentLine);

						int xLabel = OpcodesColumn - asicLabelSize - 3;

						if (xLabel < 0)
						{
							asicLabelCurrentLine += -xLabel;
							xLabel = 0;
						}

						printw(" (%s)", asicLabelCurrentLine);
					}
				}
			}
			
			D2;
		}

		// OPCODE DD364578
		// format: "0123: DD364567 LD   (IX+$45),$67"
		/*if (ShowDetails && (OpcodesColumn != -1)) // is the column hidden? (because UI is too small)
		{
			memcpy(byteDump, &session_tmpstr[6], 8);
			byteDump[8] = 0;
			
			E1;
			mvprintw(ui_y, OpcodesColumn, byteDump); 
			D1;
			
		}*/

		ui_y++;
		if (ui_y == ui_max_y)
		{
			return;
		}
	}
}

void OnDisKeyUp()
{
	WORD o, p;

	o=z80_debug_pnl0_w-4,p=z80_debug_pnl0_w-1; // longest+shortest possible operation
	while (o!=p&&z80_dasm(session_tmpstr,o)!=z80_debug_pnl0_w)
		++o;
	z80_debug_pnl0_w=o;

	z80_dasm(session_tmpstr,z80_debug_pnl0_w);
	while (session_tmpstr[6+z80_debug_pnl0_x]==' ') // *!* dirty way to ensure that the cursor is valid!
		z80_debug_pnl0_x-=2;
}

void OnDisKeyDown()
{
	WORD o, p;

	z80_debug_pnl0_w = DebugCache[1];

	z80_dasm(session_tmpstr,z80_debug_pnl0_w);
	while (session_tmpstr[6+z80_debug_pnl0_x]==' ') // *!* dirty way to ensure that the cursor is valid!
		z80_debug_pnl0_x-=2;
}

void OnDisKeyPageDown()
{
	WORD o, p;

	z80_debug_pnl0_w = DebugCache[DISASSEMBLER_PAGE_UP_DOWN_SCROLL_SPEED];

	z80_dasm(session_tmpstr,z80_debug_pnl0_w);
	while (session_tmpstr[6+z80_debug_pnl0_x]==' ') // *!* dirty way to ensure that the cursor is valid!
		z80_debug_pnl0_x-=2;
}