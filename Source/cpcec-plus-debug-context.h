#pragma once

unsigned short GetPalColor(int i)
{
	VIDEO_UNIT c = video_clut[i];

	unsigned int palColor = 0;
	palColor |= (unsigned short)( (c>>(0+4))&15);
	palColor |= (unsigned short)(((c>>(8+4))&15)<<4);
	palColor |= (unsigned short)(((c>>(16+4))&15)<<8);
	return (unsigned short)palColor;
}

void PrintValueWithMonitor(unsigned short w)
{
	E1;printw("#%04X", w);D1;
	
	/*E0;printw("->");D0;
	
	for (int i=0; i<4; i++)
	{
		BYTE b = z80_debug_peek(z80_debug_peekpoke, w + i);
		
		if (i != 0)
		{
			printw("");
		}

		E1;printw("%02X", b);D1;
	}*/
}

void Print8BitValue(unsigned char b)
{
	E1;printw("#%02X ", b);D1;
	
	/*E0;printw("(");D0;	
	int i = 0;
	for (; i<8; i++)
	{
		if (b & (128 >> i))
		{
			E1;printw("1");D1;
		}
		else
		{
			E1;printw("0");D1;
		}
	}	
	E0;printw(")");D0;*/
}

void Print8BitBinaryValue(unsigned char b)
{
	E1;printw("#%02X ", b);D1;
	
	E0;printw("(");D0;	
	int i = 0;
	for (; i<8; i++)
	{
		if (b & (128 >> i))
		{
			E1;printw("1");D1;
		}
		else
		{
			E1;printw("0");D1;
		}
	}	
	E0;printw(")");D0;
}

void PrintTitle(int y, int x, char* title)
{
	E0;mvprintw(y, x, "");D0;
	int startX = x;
	
	//if (y == 0)
	{
		E0;addch(ACS_ULCORNER);D0;x++;
	}
	/*else
	{
		E0;addch(ACS_LTEE);D0;x++;
	}*/
	for (int i=0; i<2; i++)
	{
		E0;addch(ACS_HLINE);D0;x++;
	}
	E1;printw(" %s ", title);D1;
	x += strlen(title)+2;
	while (x < WindowWidth-1)
	{
		E0;addch(ACS_HLINE);D0;x++;
	}
	if (y == 0)
	{
		E0;addch(ACS_URCORNER);D0;x++;
	}
	else
	{
		//E0;addch(ACS_RTEE);D0;x++;
		E0;addch(ACS_URCORNER);D0;x++;
	}
	
	E0;mvprintw(y, startX, "");D0;
}
void PrintLeftLine(int y, int x)
{
	E0;mvprintw(y, x, "");D0;
	E0;addch(ACS_VLINE);D0;
}
void PrintRightLine(int y)
{
	E0;mvprintw(y, WindowWidth-1, "");D0;
	E0;addch(ACS_VLINE);D0;
}
void PrintHorizontalLine(int y, int x)
{
	E0;mvprintw(y, x, "");D0;
	int startX = x;
	
	E0;addch(ACS_LLCORNER);D0;x++;
	while (x < WindowWidth-1)
	{
		E0;addch(ACS_HLINE);D0;x++;
	}
	E0;addch(ACS_LRCORNER);D0;
}

void PrintLastHorizontalLine(int y, int x)
{
	E0;mvprintw(y, x, "");D0;
	int startX = x;
	
	E0;addch(ACS_LLCORNER);D0;x++;
	while (x < WindowWidth)
	{
		E0;addch(ACS_HLINE);D0;x++;
	}
	//E0;addch(ACS_LRCORNER);D0;
}

void PrintFRegister(unsigned char f)
{
	// SZ-H-VNC
	
	E1;	
	if (f & (128 >> 0))
	{
		printw("S");
	}
	else
	{
		printw("-");
	}	
	if (f & (128 >> 1))
	{
		printw("Z");
	}
	else
	{
		printw("-");
	}	
	if (f & (128 >> 3))
	{
		printw(" H");
	}
	else
	{
		printw(" -");
	}	
	if (f & (128 >> 5))
	{
		printw(" V");
	}
	else
	{
		printw(" -");
	}		
	if (f & (128 >> 6))
	{
		printw("N");
	}
	else
	{
		printw("-");
	}	
	if (f & (128 >> 7))
	{
		printw("C");
	}
	else
	{
		printw("-");
	}	
	D1;
}

void PrintTimer()
{
	int x = TerminalX;
	
	int frameCount = 0;
	int v = (int)main_t;
	while (v >= 19968)
	{
		v -= 19968;
		frameCount++;
	};
	int remainingNops = v;
	int scanlines = remainingNops>> 6; // div 64
	
	PrintTitle(TerminalY++, x, "Timing");
	PrintLeftLine(TerminalY, x);

	E0;printw("NOPs="); D0;E1;printw("%05d", remainingNops);D1;
	E0;printw(" Lines=");D0;E1;printw("%03d", scanlines);D1;
	E0;printw(" Frames=");D0;E1;printw("%d", frameCount);D1;	
	
	PrintRightLine(TerminalY++);
	
	PrintLeftLine(TerminalY, x);
	E0;mvprintw(TerminalY, x+1, "Monitor Scanline=");D0;E1;printw("%d", video_pos_y>>1);D1;
	PrintRightLine(TerminalY++);
	
	PrintHorizontalLine(TerminalY++, x);
}

void PrintGateArray()
{
	int x = TerminalX;
	int x2 = TerminalX+20;
	
	PrintTitle(TerminalY++, x, "Gate Array");

	PrintLeftLine(TerminalY, x);
	//E0;mvprintw(TerminalY, x+1, "RAM Config=");D0;E1;printw("%dKB", gate_ram_kbyte[gate_ram_depth]);
//	if (gate_ram_depth != 0) // if config != 64KB
	{
		E0;mvprintw(TerminalY, x+1, "Bank=");D0;E1;printw("#%02X", 0xC0+(gate_ram & 127));
	}
	PrintRightLine(TerminalY++);
	
	if (plus_enabled)
	{
		x = TerminalX;
		x2 = TerminalX + 26;
		
		
		// Lower Rom
		int lowerRomIndex = plus_gate_mcr&7;
		PrintLeftLine(TerminalY, x);
		E0;mvprintw(TerminalY, x+1, "LowerROM=");D0;E1;printw("#%02X", 0x80+lowerRomIndex);D1;
		
		// Lower Rom label
		char *romLabel = GetRomLabel(lowerRomIndex);
		if (romLabel != 0)
		{
			E1;printw(" ");D1;
			E1;printw(romLabel);D1;
		}
		PrintRightLine(TerminalY++);
			
		// Lower Rom Page		
		PrintLeftLine(TerminalY, x);
		if (!(gate_mcr&4))
		{
			PushGreen();
			printw("LowerROM ENABLED at ");
			if ((plus_gate_mcr&24)==8)
			{
				printw("#4000");
			}
			else if ((plus_gate_mcr&24)==16)
			{
				printw("#8000");
			}
			else
			{
				printw("#0000");
			}	
			PopGreen();
		}
		else
		{
			PushRed();
			printw("LowerROM DISABLED");
			PopRed();
		}
		PrintRightLine(TerminalY++);
	
		// Upper Rom
		PrintLeftLine(TerminalY, x);
		if (gate_mcr&8)
		{
			E0;mvprintw(TerminalY, x+1, "UpperROM=");D0;E1;printw("OFF");D1;
		}
		else
		{
			int upperRomIndex = gate_rom - 0x80;
			E0;mvprintw(TerminalY, x+1, "UpperROM=");D0;E1;printw("#%02X", upperRomIndex + 0x80);D1;
			
			// Upper Rom label
			char *romLabel = GetRomLabel(upperRomIndex);
			if (romLabel != 0)
			{
				E1;printw(" ");D1;
				E1;printw(romLabel);D1;
			}
		}
		
		PrintRightLine(TerminalY++);
	}
	else
	{
		PrintLeftLine(TerminalY, x);
		E0;mvprintw(TerminalY, x+1, "LowerROM=");D0;E1;printw("%s", (gate_mcr&4)?"OFF":"ON");
		E0;mvprintw(TerminalY++, x2, "UpperROM=");D0;E1;printw("%s", (gate_mcr&8)?"OFF":"ON");
		PrintRightLine(TerminalY++);
	}
	
	PrintLeftLine(TerminalY, x);
	E0;mvprintw(TerminalY, x+1, "VideoMode=");D0;E1;printw("%d", (gate_status & 3));
	E0;mvprintw(TerminalY, x+14, "Pen=");D0;E1;printw("%d", gate_index);
	PrintRightLine(TerminalY++);
		
	PrintHorizontalLine(TerminalY++, x);
}

void PrintASIC()
{
	if (!plus_enabled)
	{
		return;
	}
	int x = TerminalX;
			
	PrintTitle(TerminalY++, x, "ASIC");

	PrintLeftLine(TerminalY, x);
	if ((plus_gate_mcr&24)==24)
	{
		PushGreen();mvprintw(TerminalY, x+1, "Page ENABLED");D0;PopGreen();
	}
	else
	{
		PushRed();mvprintw(TerminalY, x+1, "Page DISABLED");D0;PopRed();
	}
	PrintRightLine(TerminalY++);
	
	PrintLeftLine(TerminalY, x);
	E0;mvprintw(TerminalY, x+1, "PRI=");D0;E1;printw("%d", plus_pri);D1;
	//E0;mvprintw(TerminalY, x+10, "SPLT=");D0;E1;printw("%d", plus_sssl);D1;
	PrintRightLine(TerminalY++);
	
	PrintLeftLine(TerminalY, x);
	E0;mvprintw(TerminalY, x+1, "SPLT=");D0;E1;printw("%d", plus_sssl);D1;
	PrintRightLine(TerminalY++);
	
	PrintLeftLine(TerminalY, x);
	E0;printw("SSA=");D0;E1;printw("#%02X%02X", plus_ssss[0], plus_ssss[1]);D1;
	unsigned short vma = (plus_ssss[0]&48)*1024+(plus_ssss[0]&3)*512+plus_ssss[1]*2;
	E0;printw(" pointing to ");D0;E1;printw("#%04X", vma);D1;
	PrintRightLine(TerminalY++);
	
	PrintLeftLine(TerminalY, x);
	E0;mvprintw(TerminalY, x+1, "IVR=");D0;Print8BitBinaryValue(plus_ivr);
	PrintRightLine(TerminalY++);
	
	PrintLeftLine(TerminalY, x);
	E0;mvprintw(TerminalY, x+1, "DCSR=");D0;Print8BitBinaryValue(plus_dcsr);D1;
	PrintRightLine(TerminalY++);
	
	PrintLeftLine(TerminalY, x);
	E0;mvprintw(TerminalY, x+1, "SSCR=");D0;Print8BitBinaryValue(plus_sscr);D1;
	PrintRightLine(TerminalY++);
	
	PrintHorizontalLine(TerminalY++, x);
}

void PrintASICPalettesAndSprites()
{
	if (!plus_enabled)
	{
		return;
	}

	int startX = WindowWidth-59;
	int x = startX;
	
	E0;mvprintw(TerminalY, x, "");D0;
	E0;addch(ACS_ULCORNER);D0;x++;
	while (x < WindowWidth-36)
	{
		E0;addch(ACS_HLINE);D0;x++;
	}
	E0;addch(ACS_BTEE);D0;x++;
	while (x < WindowWidth)
	{
		E0;addch(ACS_HLINE);D0;x++;
	}
//	E0;addch(ACS_RTEE);D0;x++;
	TerminalY++;
	
	x = startX;
	E2;mvprintw(TerminalY, x, "");D2;
	E0;addch(ACS_VLINE);D0;
	E2;printw("    PAL0-7 ");D2;
	for (int i=0; i<8; i++)
	{
		E0;printw("%02d", i);D0;E1;printw("%03X ", GetPalColor(i));D1;
	}
	E2;mvprintw(TerminalY++, WindowWidth-1, "");D2;
	//E0;addch(ACS_VLINE);D0;
	
	E2;mvprintw(TerminalY, x, "");D2;
	E0;addch(ACS_VLINE);D0;
	E2;printw("   PAL8-15 ");D2;
	for (int i=8; i<16; i++)
	{
		E0;printw("%02d", i);D0;E1;printw("%03X ", GetPalColor(i));D1;
	}
	E2;mvprintw(TerminalY++, WindowWidth-1, "");D2;
	//E0;addch(ACS_VLINE);D0;
	
	E2;mvprintw(TerminalY, x, "");D2;
	E0;addch(ACS_VLINE);D0;
	E2;printw(" SPRPAL1-7       ");D2;
	for (int i=1; i<8; i++)
	{
		E0;printw("%02d", i);D0;E1;printw("%03X ", GetPalColor(16+i));D1;
	}
	E2;mvprintw(TerminalY++, WindowWidth-1, "");D2;
	//E0;addch(ACS_VLINE);D0;
	
	E2;mvprintw(TerminalY, x, "");D2;
	E0;addch(ACS_VLINE);D0;	
	E2;printw("SPRPAL8-15 ");D2;
	for (int i=8; i<16; i++)
	{
		E0;printw("%02d", i);D0;E1;printw("%03X ", GetPalColor(16+i));D1;
	}
	E2;mvprintw(TerminalY++, WindowWidth-1, "");D2;
	//E0;addch(ACS_VLINE);D0;
	
	
	x = startX;
	
	E0;mvprintw(TerminalY, x, "");D0;
	E0;addch(ACS_PLUS);D0;x++;
	while (x < WindowWidth-56)
	{
		E0;addch(ACS_HLINE);D0;x++;
	}
	E0;addch(ACS_TTEE);D0;x++;
	while (x < WindowWidth)
	{
		E0;addch(ACS_HLINE);D0;x++;
	}
	//E0;addch(ACS_RTEE);D0;x++;
	TerminalY++;
	
	startX += 3;
		
	for (int spriteIndex=0; spriteIndex<16; spriteIndex++)
	{
		int i = spriteIndex*8;

		int zoomY = (plus_sprite_xyz[i+4]&3);
		int zoomX = (plus_sprite_xyz[i+4]>>2);
		int posY = (plus_sprite_xyz[i+2]+256*plus_sprite_xyz[i+3])&511; // 9-bit wrap!
		int posX = (plus_sprite_xyz[i+0]+256*(signed char)plus_sprite_xyz[i+1]);
		
		int x = startX;
		
		if (spriteIndex>=8)
		{
			x += 28;
		}
		int y = TerminalY;
		if (spriteIndex < 8)
		{
			y += spriteIndex;
		}
		else
		{
			y += spriteIndex-8;
		}

		E2;mvprintw(y, x, "");D2;
		if (spriteIndex<8)
		{
			E0;addch(ACS_VLINE);D0;
			E2;printw("SPR%02d", spriteIndex);D2;
		}	
		else
		{
			E2;printw("SPR%02d", spriteIndex);D2;		
		}
		E1;printw(" %d", zoomX);D1;E0;printw("x");D0;E1;printw("%d", zoomY);D1;
		E0;printw(" X=");D0;E1;printw("%d", posX);D1;
		E0;printw(" Y=");D0;E1;printw("%d", posY);D1;

		if (spriteIndex>=8)
		{
			E2;mvprintw(y, WindowWidth-1, "");D2;
			//E0;addch(ACS_VLINE);D0;
		}
	}
	
	TerminalY += 8;
	
	/*x = startX;
	
	E0;mvprintw(TerminalY, x, "");D0;
	E0;addch(ACS_LLCORNER);D0;x++;
	while (x < WindowWidth-1)
	{
		E0;addch(ACS_HLINE);D0;x++;
	}
	E0;addch(ACS_LRCORNER);D0;x++;
	TerminalY++;*/
}

void PrintMonitorCloseToASIC()
{
	unsigned short address = MonitorAddress;
	
	int startX = 0;
	int x = startX;
	int y = TerminalY - 9;
	
	E0;mvprintw(y, x, "");D0;
	//E0;addch(ACS_ULCORNER);D0;x++;
	while (x < WindowWidth-59)
	{
		E0;addch(ACS_HLINE);D0;x++;
	}
	E0;addch(ACS_BTEE);D0;x++;
	
	x = startX;	
	y++;

	while (y < WindowHeight)
	{
		E0;mvprintw(y, x, "");D0;
		//E0;addch(ACS_VLINE);D0;
		E0;printw("#%04X", address);

		for (int i=0; i<16; i++)
		{
			BYTE b = z80_debug_peek(z80_debug_peekpoke, address + i);

			//if ((i&1)==0)
			{
				E1;printw(" %02X", b);D1;
			}
			//else
			{
				//E0;printw(" %02X", b);D0;
			}
		}

		address += 16;

		y++;
	}
	
	/*E0;mvprintw(y, x, "");D0;
	E0;addch(ACS_LLCORNER);D0;x++;
	while (x < WindowWidth-54)
	{
		E0;addch(ACS_HLINE);D0;x++;
	}
	E0;addch(ACS_BTEE);D0;x++;*/
}
	
void PrintCRTCIndex(int regIndex)
{
	if (regIndex == crtc_index)
	{
		E1;
	}
	else
	{
		E0;
	}
	printw("%02d ", regIndex);
	if (regIndex == crtc_index)
	{
		D1;
	}
	else
	{
		D0;
	}
}

void PrintCRTCReg(int regIndex)
{
	if (regIndex == crtc_index)
	{
		E1;
	}
	else
	{
		E0;
	}
	printw("%02X ", crtc_table[regIndex]);
	if (regIndex == crtc_index)
	{
		D1;
	}
	else
	{
		D0;
	}
}

void PrintCRTC()
{
	int x = TerminalX;
	int x2 = x + 10;
	int x3 = x2 + 10;
	int x4 = x3 + 10;
	
	if (crtc_type == 0)
	{
		PrintTitle(TerminalY++, x, "CRTC Type 0");
	}
	else if (crtc_type == 1)
	{
		PrintTitle(TerminalY++, x, "CRTC Type 1");
	}
	else if (crtc_type == 2)
	{
		PrintTitle(TerminalY++, x, "CRTC Type 2");
	}
	else if (crtc_type == 3)
	{
		PrintTitle(TerminalY++, x, "CRTC Type 3");
	}
	else if (crtc_type == 4)
	{
		PrintTitle(TerminalY++, x, "CRTC Type 4");
	}
	
	WORD w;
	WORD m = z80_sp.w;
	
	PrintLeftLine(TerminalY, x);
	PrintCRTCIndex(0);
	PrintCRTCIndex(1);
	PrintCRTCIndex(2);
	PrintCRTCIndex(3);
	PrintCRTCIndex(4);
	PrintCRTCIndex(5);
	PrintCRTCIndex(6);
	PrintCRTCIndex(7);
	PrintCRTCIndex(9);
	PrintCRTCIndex(12);
	PrintCRTCIndex(13);
	PrintRightLine(TerminalY++);
	
	PrintLeftLine(TerminalY, x);
	PrintCRTCReg(0);
	PrintCRTCReg(1);
	PrintCRTCReg(2);
	PrintCRTCReg(3);
	PrintCRTCReg(4);
	PrintCRTCReg(5);
	PrintCRTCReg(6);
	PrintCRTCReg(7);
	PrintCRTCReg(9);
	PrintCRTCReg(12);
	PrintCRTCReg(13);
	PrintRightLine(TerminalY++);
		
	PrintLeftLine(TerminalY, x);
	E0;printw("VCC=");D0;E1;printw("#%02X", crtc_count_r4);D1;
	E0;printw(" VLC=");D0;E1;printw("#%02X", crtc_count_r9);D1;
	E0;printw(" HCC=");D0;E1;printw("#%02X", crtc_count_r0);D1;
	E0;printw(" VMA=");D0;E1;printw("#%04X", crtc_screen+crtc_raster);D1;
	PrintRightLine(TerminalY++);
	
	PrintHorizontalLine(TerminalY++, x);
}

void PrintZ80Registers()
{
	int x = TerminalX;	
		
	PrintTitle(TerminalY++, x, "Z80 CPU");
	
	PrintLeftLine(TerminalY, x);
	E0;printw("SZ-H-VNC  ");D1;	
	PrintRightLine(TerminalY++);
	
	PrintLeftLine(TerminalY, x);
	PrintFRegister(z80_af.b.l);
	PrintRightLine(TerminalY++);
	
	PrintLeftLine(TerminalY, x);
	E0;printw("AF=");D0;E1;printw("#%04X", z80_af.w);D1;	
	E0;printw("  AF'=");D0;E1;printw("#%04X", z80_af2.w);D1;
	PrintRightLine(TerminalY++);

	PrintLeftLine(TerminalY, x);
	E0;printw("HL=");D0;E1;PrintValueWithMonitor(z80_hl.w);D1;	
	E0;printw("  HL'=");D0;E1;PrintValueWithMonitor(z80_hl2.w);D1;
	PrintRightLine(TerminalY++);
	
	PrintLeftLine(TerminalY, x);
	E0;printw("DE=");D0;E1;PrintValueWithMonitor(z80_de.w);D1;
	E0;printw("  DE'=");D0;E1;PrintValueWithMonitor(z80_de2.w);D1;
	PrintRightLine(TerminalY++);

	PrintLeftLine(TerminalY, x);
	E0;printw("BC=");D0;E1;PrintValueWithMonitor(z80_bc.w);D1;
	E0;printw("  BC'=");D0;E1;PrintValueWithMonitor(z80_bc2.w);D1;
	PrintRightLine(TerminalY++);

	PrintLeftLine(TerminalY, x);
	E0;printw("IX=");D0;E1;PrintValueWithMonitor(z80_ix.w);D1;
	E0;printw("  SP=");D0;E1;PrintValueWithMonitor(z80_sp.w);D1;
	PrintRightLine(TerminalY++);
	
	PrintLeftLine(TerminalY, x);
	E0;printw("IY=");D0;E1;PrintValueWithMonitor(z80_iy.w);D1;
	E0;printw("  PC=");D0;E1;PrintValueWithMonitor(z80_pc.w);D1;
	PrintRightLine(TerminalY++);
	
	PrintLeftLine(TerminalY, x);
	E0;printw("IM=");D0;E1;printw("%c", '0'+z80_imd);D1;
	E0;printw("      I=");D0;E1;Print8BitValue(z80_ir.b.h);D1;
	PrintRightLine(TerminalY++);
	
	PrintLeftLine(TerminalY, x);
	if (z80_iff.b.l == 0)
	{
		PushGreen();printw("Interrupts ENABLED");PopGreen();
	}
	else
	{
		PushRed();printw("Interrupts DISABLED");PopRed();
	}
	PrintRightLine(TerminalY++);
	
	PrintHorizontalLine(TerminalY++, x);
	
	/*E0;mvprintw(TerminalY, x, "A=");D0;Print8BitValue(z80_af.w >> 8);
	E0;mvprintw(TerminalY++, x2, "A'=");D0;Print8BitValue(z80_af2.w >> 8);
	E0;mvprintw(TerminalY, x, "B=");D0;Print8BitValue(z80_bc.w >> 8);
	E0;mvprintw(TerminalY++, x2, "B'=");D0;Print8BitValue(z80_bc2.w >> 8);
	E0;mvprintw(TerminalY, x, "C=");D0;Print8BitValue(z80_bc.w & 255);
	E0;mvprintw(TerminalY++, x2, "C'=");D0;Print8BitValue(z80_bc2.w & 255);
	E0;mvprintw(TerminalY, x, "D=");D0;Print8BitValue(z80_de.w >> 8);
	E0;mvprintw(TerminalY++, x2, "D'=");D0;Print8BitValue(z80_de2.w >> 8);
	E0;mvprintw(TerminalY, x, "E=");D0;Print8BitValue(z80_de.w & 255);
	E0;mvprintw(TerminalY++, x2, "E'=");D0;Print8BitValue(z80_de2.w & 255);
	E0;mvprintw(TerminalY, x, "H=");D0;Print8BitValue(z80_hl.w >> 8);
	E0;mvprintw(TerminalY++, x2, "H'=");D0;Print8BitValue(z80_hl2.w >> 8);
	E0;mvprintw(TerminalY, x, "L=");D0;Print8BitValue(z80_hl.w & 255);	
	E0;mvprintw(TerminalY++, x2, "L'=");D0;Print8BitValue(z80_hl2.w & 255);
	TerminalY++;*/
}

void PrintStack()
{	
	int x = TerminalX;// + 11;
	int y = TerminalY;
	
	WORD w;
	WORD m = z80_sp.w;
	for (int i=0; i<6; i++)
	{
		if (y == WindowHeight)
		{
			break;
		}
		
		E0;mvprintw(y++, x, "#%04X ", m);D0;
		w=PEEK(m); ++m;
		w+=PEEK(m)<<8; ++m;
		E1;PrintValueWithMonitor(w);D1;
		
		TerminalY++;
	}
	
	//TerminalY++;
}
