
#pragma once

// Note: exposed to cpcec, everything here should be minimal

#include <SDL2/SDL.h> // requires defining the symbol SDL_MAIN_HANDLED!

// Headers
#include <ncurses.h> // requires sudo apt-get install libncurses5-dev
#include <sys/ioctl.h>
#include <string.h>
#include <stdio.h> // file io
#include <stdlib.h> // strtol
#include <string.h> // strcpy

// Globals
int DefaultSDLWindowX = SDL_WINDOWPOS_UNDEFINED;
int DefaultSDLWindowY = SDL_WINDOWPOS_UNDEFINED;
bool HasFocus = false;

bool HasSymbolFilename = false;
bool HasRomLabelsFilename = false;
char SymbolFilename[256] = {0};
char RomLabelsFilename[256] = {0};
char AnyLoadFilename[256] = {0};

char* GetLabel(unsigned short address);

// Function declarations
void TerminalInit();
void TerminalExit();
bool TerminalUpdate(); // returns true if quit is requested

// Draw sprites into the video buffer if required
// Returns true if rendering has been processed (cf. if Sprites mode is active in Terminal)
bool DrawSDLSpriteViewer();

// Draw palette into the video buffer if required
// Returns true if rendering has been processed (cf. if Palette mode is active in Terminal)
bool DrawSDLPaletteViewer();

void SwitchTerminalToRunningEmulation();
void SwitchTerminalToDebugger();
void SwitchTerminalToSpriteViewer();
