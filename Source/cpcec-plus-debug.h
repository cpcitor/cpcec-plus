
#pragma once

static int TerminalX = 0;
static int TerminalY = 0;
static bool MonitorAddressSet = false;
static bool MonitorIsPreviousValueEntered = false;
static unsigned short EnteredMonitorAddress;
static unsigned short MonitorAddress = 0x8000;
static unsigned short DisAddress = 0x8000;
static bool ShowAskNewDisAddress = false;
static bool ShowExportMemoryFragment = false;
static bool ShowWaitScanline = false;
static bool LabelSingleLine = false;
static bool ShowDetails = false;

#define COLUMN_WIDTH 0
#define DISASSEMBLER_PAGE_UP_DOWN_SCROLL_SPEED 8

WORD DebugCache[DISASSEMBLER_PAGE_UP_DOWN_SCROLL_SPEED + 1];

int OpcodesColumn;
int ContextColumn;
int AddressColumn;

#include "cpcec-plus-symbols-asic.h"
#include "cpcec-plus-symbols.h"
#include "cpcec-plus-debug-input.h"
#include "cpcec-plus-debug-context.h"
#include "cpcec-plus-debug-disasm.h"
#include "cpcec-plus-debug-tools.h"

void DebuggerOnKeyPress()
{
	switch (PressedKey)
	{
		case 27: // escape key
		case KEY_F(5):
			session_signal&=~SESSION_SIGNAL_DEBUG;
			SwitchTerminalToRunningEmulation();
			break;

		case ' ': // space key
		case KEY_F(10):
			z80_main(1),z80_debug_reset();
			break;

		case KEY_F(9): // toggle breakpoint
		case 'B': // toggle breakpoint
		case 'b': // toggle breakpoint
			{
				WORD m=z80_debug_pnl0_w;
				/*for (int i=0; i<3; i++)
				{
					m=z80_dasm(session_tmpstr,m);
				}*/			
				WORD w=m;
				z80_breakpoints[w]=!z80_breakpoints[w];
				ForceNextRefresh = true;
			}
			break;

		case KEY_PPAGE: // page up
			if (ShowMonitor)
			{
				MonitorAddress -= 0x100;
			}
			else
			{
				for (int i=0; i<DISASSEMBLER_PAGE_UP_DOWN_SCROLL_SPEED; i++)
				{
					OnDisKeyUp();
				}
			}
			break;

		case KEY_NPAGE: // page down
			if (ShowMonitor)
			{
				MonitorAddress += 0x100;
			}
			else
			{
				OnDisKeyPageDown();
			}
			break;

		case KEY_UP: // scroll up
			if (ShowMonitor)
			{
				MonitorAddress -= 16;
			}
			else
			{
				OnDisKeyUp();
			}
			break;

		case KEY_DOWN: // scroll down
			if (ShowMonitor)
			{
				MonitorAddress += 16;
			}
			else
			{
				OnDisKeyDown();
			}
			break;

		case 'D': // set new disassembly address (NOT A JUMP!)
		case 'd':
			ShowAskNewDisAddress = true;
			break;

		case 'E': // export memory fragment
		case 'e':
			ShowExportMemoryFragment = true;
			break;

		case 'W': // export memory fragment
		case 'w':
			ShowWaitScanline = true;
			break;
			
		case '?': // toggle context details
			if (ShowDetails)
			{
				ShowDetails = false;
			}
			else
			{
				ShowDetails = true;
			}
			ForceNextRefresh = true;
			break;


		case 'M': // SHOW MONITOR
		case 'm':
			MonitorAddressSet = false;
			ShowMonitor = !ShowMonitor;
			ShowSpriteViewer = false;
			ShowPaletteViewer = false;
			ForceNextRefresh = true;

			// Force refresh SDL window next frame
			debug_buffer[0]=128; // 128: draw debug screen
			session_signal|=SESSION_SIGNAL_DEBUG;
			break;

		case 'P': // SHOW PALETTE VIEWER
		case 'p':
			ShowPaletteViewer = !ShowPaletteViewer;
			ShowMonitor = false;
			ShowSpriteViewer = false;
			ForceNextRefresh = true;

			// Force refresh SDL window next frame
			debug_buffer[0]=128; // 128: draw debug screen
			session_signal|=SESSION_SIGNAL_DEBUG;
			break;

		case 'S': // SHOW SPRITE VIEWER
		case 's':
			ShowSpriteViewer = !ShowSpriteViewer;
			ShowMonitor = false;
			ShowPaletteViewer = false;
			ForceNextRefresh = true;

			// Force refresh SDL window next frame
			debug_buffer[0]=128; // 128: draw debug screen
			session_signal|=SESSION_SIGNAL_DEBUG;
			break;

		case 'T': // RESET TIMER
		case 't':
			main_t = 0;
			break;

		default:
			break;
	}
}

void ShowContextPanel()
{
	TerminalX = ContextColumn+1;
	TerminalY = 0;
		
	PrintZ80Registers();
	//PrintStack();
	
	//TerminalY++;
	PrintTimer();
	PrintCRTC();
	
	//TerminalY++;
	PrintGateArray();
	
	PrintASIC();
	//PrintASICPalettesAndSprites();
	//E3;mvprintw(0, 0, "WindowHeight=%d", WindowHeight);D3;
	
	//PrintMonitorCloseToASIC();
}

void TerminalShowDebugger()
{
	if (PressedKey != -1)
	{
		DebuggerOnKeyPress();
	}

	/*
	// scroll up few lines in debugger, so we can see past instructions
	switch (PressedKey)
	{
		case 27: // escape key
		case KEY_F(5):			
		case ' ': // space key
		case KEY_F(10):
			for (int i=0; i<3; i++)
			{
				OnDisKeyUp();
			}
		default:
			break;
	}*/

	bool rightPanelShown = false;

	int yDis = 0;

	ContextColumn = WindowWidth - 37;
	OpcodesColumn = ContextColumn - 10;

	if (WindowWidth < 140)
	{
		LabelSingleLine = true;
	}
	else
	{
		LabelSingleLine = false;
	}
	
	/*if (WindowWidth < 110)
	{
		ShowDetails = false;
	}
	else
	{
		ShowDetails = true;	
	}*/

	AddressColumn = (int)((0.9f*(float)OpcodesColumn)) / 2;
	const int MaxSizeInstructionWithLabel = 40;
	if ((AddressColumn + MaxSizeInstructionWithLabel) > OpcodesColumn)
	{
		AddressColumn = OpcodesColumn - MaxSizeInstructionWithLabel;
	}

	if (ShowMonitor)
	{
		if (MonitorAddressSet == false)
		{
			PrintZ80Disassembly(AddressColumn, yDis, WindowHeight-1);

			ShowContextPanel();
			rightPanelShown = true;

			TerminalY = WindowHeight-1;

			AskMonitorAddress();
		}
		else
		{
			PrintZ80Disassembly(AddressColumn, yDis, WindowHeight-8);
			TerminalY = WindowHeight-8;
			PrintMonitor();
		}
	}
	else if (ShowSpriteViewer)
	{
		PrintZ80Disassembly(AddressColumn, yDis, WindowHeight-8);
		TerminalY = WindowHeight-8;
		PrintASICSprites();
	}
	else if (ShowPaletteViewer)
	{
		PrintZ80Disassembly(AddressColumn, yDis, WindowHeight-4);
		TerminalY = WindowHeight-4;
		PrintPalette();
	}
	else
	{
		PrintZ80Disassembly(AddressColumn, yDis, WindowHeight);
	}

	if (rightPanelShown == false)
	{
		ShowContextPanel();
	}

	if (ShowAskNewDisAddress)
	{
		ShowAskNewDisAddress = false;

		AskNewDisAddress();
	}
	else if (ShowWaitScanline)
	{
		ShowWaitScanline = false;

		AskWaitScanline();
	}
	else if (ShowExportMemoryFragment)
	{
		ShowExportMemoryFragment = false;

		ExportMemoryFragment();
	}
}