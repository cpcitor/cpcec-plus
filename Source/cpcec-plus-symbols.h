
#pragma once

char* GetLabel(unsigned short address)
{
	char *label = 0;
	if (address < (unsigned short)0xC000)
	{
		label = RAMSymbols[ address ];
	}
	else if (
			((gate_rom - 0x80) >= 0) 
		&& 	((gate_rom - 0x80) < 32)
		&&	((address) >= (unsigned short)0xC000)
		&&	((address) <= (unsigned short)0xFFFF))
	{
		label = ROMSymbols[ gate_rom - 0x80 ][ address - 0xC000 ];
	}
	return label;
}

void LoadSymbols()
{
	// Clear all symbols
	memset(&RAMSymbols[0], 0, sizeof(char*) * 16384*3); // [0000-BFFF]
	memset(&ROMSymbols[0][0], 0, sizeof(char*) * 32 * 16384); // [C000-FFFF]*32 roms

	if (!HasSymbolFilename)
	{
		return;
	}

	FILE* fileStream = fopen(SymbolFilename, "r");
    if (fileStream == NULL)
	{
        return;
    }

    // Parse RAM/ROM symbols
	char tmp[256];
	while (fgets(tmp, 256, fileStream) != NULL)
	{
		char* p = strchr(tmp, ':');
		if (p == NULL)
		{
			// label not found, skipping
			continue;
		}

		char *end = strchr(p, '\n');
		if (end == NULL)
		{
			// label not found, skipping
			continue;
		}
		*end = 0;

		// rom index
		*p = 0;
		unsigned char romIndex = (unsigned char)strtol(tmp, NULL, 10);

		// breakpoint address
		p++;
		*(p+4) = 0;
		unsigned short address = (unsigned short)strtol(p, NULL, 16);

		// hack to skip rasm's structure bug in symbols
		if (address < 0x400)
		{
			continue;
		}

		// label
		char* label = p+5;

		unsigned char labelSize = strlen(label);

		char *data = malloc(labelSize + 1);
		strcpy(data, label);

		if (address < 0xC000)
		{
			RAMSymbols[ address ] = data;
		}
		else
		{
			ROMSymbols[ romIndex ][ address - 0xC000 ] = data;
		}
	}

    fclose(fileStream);
}

char* GetRomLabel(unsigned char romNumber) // romNumber is starting from 0, not 0x80
{
	char *romLabel = 0;
	if ((romNumber >= 0) && (romNumber < 32))
	{
		romLabel = ROMLabels[ romNumber ];
	}
	return romLabel;
}

void LoadRomLabels()
{
	// Clear all rom labels
	memset(&ROMLabels[0], 0, sizeof(char*) * 32); // max. 32 roms (cf. a regular cartridge)

	if (!HasRomLabelsFilename)
	{
		return;
	}

	FILE* fileStream = fopen(RomLabelsFilename, "r");
    if (fileStream == NULL)
	{
        return;
    }

    // Parse ROM labels
	char tmp[256];
	while (fgets(tmp, 256, fileStream) != NULL)
	{
		char* p = strchr(tmp, ':');
		if (p == NULL)
		{
			// label not found, skipping
			continue;
		}

		char *end = strchr(p, '\n');
		if (end == NULL)
		{
			// label not found, skipping
			continue;
		}
		*end = 0;

		// rom index
		*p = 0;
		unsigned char romIndex = (unsigned char)strtol(tmp, NULL, 10);

		// label
		char* label = p+2; // skip ':' and ' '

		unsigned char labelSize = strlen(label);

		char *data = malloc(labelSize + 1);
		strcpy(data, label);

		ROMLabels[ romIndex ] = data;
	}

    fclose(fileStream);
}