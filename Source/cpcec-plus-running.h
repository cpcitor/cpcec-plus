
#pragma once

void TerminalShowEmulation()
{
	E1;printw("Emulation core: ");D1;E1;printw(MY_CAPTION " [ncurses-debug] " MY_VERSION "\n");D1;
	E1;printw("Session info: ");D1;E1;printw("%s\n\n", session_info);D1;
	E1;printw("Specified file: ");D1;E1;printw("%s\n", AnyLoadFilename);D1;
	if (HasSymbolFilename)
	{
		E1;printw("Labels file: ");D1;E1;printw("%s\n", SymbolFilename);D1;
	}
	else
	{
		E1;printw("No symbols file specified\n");D1;
	}
	if (HasRomLabelsFilename)
	{
		E1;printw("Rom Labels file: ");D1;E1;printw("%s\n\n", RomLabelsFilename);D1;
	}
	else
	{
		E1;printw("No Rom Labels file specified\n\n");D1;
	}
	D0;

	E2;printw("Emulation is running...\n\n");D2;

	E1;printw("While in emulation mode:\n");D1;
	E1;
	printw(
		"  Q to quit\n"
		"  ESC or Space to pause emulation and enter debugger\n\n");
	D1;


	E1;printw("When in debugger:\n");D1;
	E1;
	printw(
		"  Q to quit\n"
		"  F5 or ESC to get back to emulation\n"
		"  F9 or B to toggle breakpoint\n"
		"  F10 or Space for 'debug step into'\n"
		"  ? to show contextual details of disassembled code\n"
		"  T to reset NOPS counter\n"
		"  D to set new disassembly address\n"
		"  S to toggle Sprite Viewer\n"
		"  P to toggle Palette Viewer\n"
		"  M to toggle Monitor View (then Up/Down/PgUp/PgDown to navigate in dumped memory)\n"
		"  W to run Z80 until specified scanline is reached\n"
		"  E to export memory fragment as a binary file\n"
		"  Up/Down/PgUp/PgDown to navigate in disassembled code\n\n"
	);
	D1;

	switch (PressedKey)
	{
		case ' ': // space key
		case 27: // escape key
			z80_debug_reset(); session_signal|=SESSION_SIGNAL_DEBUG;
			SwitchTerminalToDebugger();
			break;

		default:
			break;
	}
}