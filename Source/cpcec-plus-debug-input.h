
#pragma once

unsigned short AskHexadecimalAddress(bool HasDefaultValue, unsigned short Address)
{
	E1;
	curs_set(2);

	char str[5];
	str[4] = 0;

	int index = 0;

	if (HasDefaultValue)
	{
		sprintf(str, "%04X", Address);
		printw(str);
		index = 4;
	}

	bool quit = false;
	while (!quit)
	{
		int c = getch();
		if (c == KEY_BACKSPACE)
		{
			if (index>0)
			{
				int x, y;
				getyx(stdscr, y, x);
				move(y, x-1);
				addch(' ');
				move(y, x-1);
				index--;
			}
		}
		else if (((c>='A') && (c<='F'))
		||  ((c>='a') && (c<='f'))
		||  ((c>='0') && (c<='9')))
		{
			// force uppercase
			if ((c>='a') && (c<='f'))
			{
				c -= 'a' - 'A';
			}

			if (index < 4)
			{
				addch(c); // print character

				str[index] = c;
				index++;
			}
		}
		else if (c == 10) // KEY_ENTER
		{
			if (index == 4)
			{
				quit = true;
			}
		}
	}

	unsigned short address = (unsigned short)strtol(str, NULL, 16);

	curs_set(0);
	D1;

	return address;
}

void AskString(char* FillStr, int maxSize)
{
	E1;
	curs_set(2);

	int index = 0;

	bool quit = false;
	while (!quit)
	{
		int c = getch();
		if (c == KEY_BACKSPACE)
		{
			if (index>0)
			{
				int x, y;
				getyx(stdscr, y, x);
				move(y, x-1);
				addch(' ');
				move(y, x-1);

				FillStr[index] = 0;
				index--;
			}
		}
		else if ((c>=32) && (c<=127))
		{
			if (index < maxSize-1) // -1 because null string terminator
			{
				addch(c); // print character

				FillStr[index] = c;
				index++;
				FillStr[index] = 0;
			}
		}
		else if (c == 10) // KEY_ENTER
		{
			quit = true;
		}
	}

	curs_set(0);
	D1;
}
