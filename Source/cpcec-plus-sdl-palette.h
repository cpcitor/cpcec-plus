
#pragma once

#define COLOR_SPACE_SIZE 14
#define COLOR_BLOCK_SIZE 64

void ShowColorLine(int LineNumber, int PenStart, int XOffset, int PenCount)
{
	int y = COLOR_SPACE_SIZE + LineNumber * (COLOR_SPACE_SIZE+COLOR_BLOCK_SIZE);
	int x = COLOR_SPACE_SIZE + (XOffset * (COLOR_SPACE_SIZE+COLOR_BLOCK_SIZE));

	for (int penIndex = PenStart; penIndex < PenStart+PenCount; penIndex++)
	{
		VIDEO_UNIT c = video_clut[penIndex];

		for (int PosY = y; PosY < y+COLOR_BLOCK_SIZE; PosY++)
		{
			for (int PosX = x; PosX < x+COLOR_BLOCK_SIZE; PosX++)
			{
				debug_frame[ (PosY * VIDEO_PIXELS_X) + PosX ] = c;
			}
		}

		x += COLOR_BLOCK_SIZE + COLOR_SPACE_SIZE;
	}
}

// Draw palette into the video buffer if required
// Returns true if rendering has been processed (cf. if Palette mode is active in Terminal)
bool DrawSDLPaletteViewer()
{
	if (!ShowPaletteViewer)
	{
		return false;
	}

	for (int y=0;y<VIDEO_PIXELS_Y;++y)
	{
		memset(&debug_frame[y*VIDEO_PIXELS_X], 0x00202020, VIDEO_PIXELS_X * sizeof(VIDEO_UNIT));
		/*for (int x=0;x<VIDEO_PIXELS_X;++x)
		{
			debug_frame[(y*VIDEO_PIXELS_X)+x] = 0x00202020; // 0x00RRGGBB
		}*/
	}

	ShowColorLine(0, 0, 0, 8); // PAL0-7
	ShowColorLine(1, 8, 0, 8); // PAL8-15
	
	if (plus_enabled)
	{
		ShowColorLine(1+2, 16+1, 1, 7); // SPRPAL1-7
		ShowColorLine(1+3, 16+8, 0, 8); // SPRPAL8-15
	}

	return true;
}
