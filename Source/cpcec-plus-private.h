
#pragma once

int WindowWidth = -1;
int WindowHeight = -1;
int PressedKey = -1;
bool ForceNextRefresh = false;

bool ShowSpriteViewer = false;
bool ShowPaletteViewer = false;
bool ShowMonitor = false;

char* RAMSymbols[16384*3]; // [0000-BFFF]
char* ROMSymbols[32][16384]; // [C000-FFFF]*32 roms
char* ROMLabels[32]; // 32 roms in a cartridge

enum ETerminalStateEnum
{
	TerminalState_ShowRunningEmulation,
	TerminalState_ShowDebugger
};
enum ETerminalStateEnum TerminalState = TerminalState_ShowRunningEmulation;
