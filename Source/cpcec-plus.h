
#pragma once

#include "cpcec-plus-private.h"
#include "cpcec-plus-colors.h"
#include "cpcec-plus-running.h"
#include "cpcec-plus-debug.h"
#include "cpcec-plus-sdl-sprites.h"
#include "cpcec-plus-sdl-palette.h"

void SwitchTerminalToRunningEmulation()
{
	if (TerminalState == TerminalState_ShowRunningEmulation)
	{
		return;
	}
	TerminalState = TerminalState_ShowRunningEmulation;
	ForceNextRefresh = true;
}

void SwitchTerminalToDebugger()
{
	if (TerminalState == TerminalState_ShowDebugger)
	{
		return;
	}
	TerminalState = TerminalState_ShowDebugger;
	ForceNextRefresh = true;
}

void TerminalInit()
{
	initscr();

	keypad(stdscr, TRUE);
	noecho();
	cbreak();
	timeout(0);
	curs_set(0);
	set_escdelay(0);

	InitTerminalColors();
	
	z80_debug_edfftrap = true;
}

void TerminalExit()
{
	endwin();
}

// returns true if quit is requested
bool TerminalUpdate()
{
	int w, h;
	getmaxyx(stdscr, h, w);

	bool doRefresh = false;

	if (WindowWidth != w)
	{
		WindowWidth = w;
		doRefresh = true;
	}

	if (WindowHeight != h)
	{
		WindowHeight = h;
		doRefresh = true;
	}

	PressedKey = getch();
	if ((PressedKey == 'q') || (PressedKey == 'Q'))
	{
		return true;
	}

	if (PressedKey != -1)
	{
		doRefresh = true;
	}

	if (ForceNextRefresh)
	{
		ForceNextRefresh = false;
		doRefresh = true;
	}

	if (doRefresh)
	{
		clear();

		switch (TerminalState)
		{
			default:
			case TerminalState_ShowRunningEmulation:
				TerminalShowEmulation();
				break;

			case TerminalState_ShowDebugger:
				TerminalShowDebugger();
				break;
		}

		refresh(); // present content to terminal
	}

	return false;
}
