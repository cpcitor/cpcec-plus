#pragma once

struct ASICLabel
{
	unsigned short adr;
	char* label;
};

struct ASICLabel Labels[] =
{
	{ 0x6000 + (0*8), "SPR. 0 X POS" },
	{ 0x6002 + (0*8), "SPR. 0 Y POS" },
	{ 0x6004 + (0*8), "SPR. 0 MAG" },	
	{ 0x6000 + (1*8), "SPR. 1 X POS" },
	{ 0x6002 + (1*8), "SPR. 1 Y POS" },
	{ 0x6004 + (1*8), "SPR. 1 MAG" },	
	{ 0x6000 + (2*8), "SPR. 2 X POS" },
	{ 0x6002 + (2*8), "SPR. 2 Y POS" },
	{ 0x6004 + (2*8), "SPR. 2 MAG" },	
	{ 0x6000 + (3*8), "SPR. 3 X POS" },
	{ 0x6002 + (3*8), "SPR. 3 Y POS" },
	{ 0x6004 + (3*8), "SPR. 3 MAG" },	
	{ 0x6000 + (4*8), "SPR. 4 X POS" },
	{ 0x6002 + (4*8), "SPR. 4 Y POS" },
	{ 0x6004 + (4*8), "SPR. 4 MAG" },	
	{ 0x6000 + (5*8), "SPR. 5 X POS" },
	{ 0x6002 + (5*8), "SPR. 5 Y POS" },
	{ 0x6004 + (5*8), "SPR. 5 MAG" },	
	{ 0x6000 + (6*8), "SPR. 6 X POS" },
	{ 0x6002 + (6*8), "SPR. 6 Y POS" },
	{ 0x6004 + (6*8), "SPR. 6 MAG" },	
	{ 0x6000 + (7*8), "SPR. 7 X POS" },
	{ 0x6002 + (7*8), "SPR. 7 Y POS" },
	{ 0x6004 + (7*8), "SPR. 7 MAG" },	
	{ 0x6000 + (8*8), "SPR. 8 X POS" },
	{ 0x6002 + (8*8), "SPR. 8 Y POS" },
	{ 0x6004 + (8*8), "SPR. 8 MAG" },	
	{ 0x6000 + (9*8), "SPR. 9 X POS" },
	{ 0x6002 + (9*8), "SPR. 9 Y POS" },
	{ 0x6004 + (9*8), "SPR. 9 MAG" },	
	{ 0x6000 + (10*8), "SPR. 10 X POS" },
	{ 0x6002 + (10*8), "SPR. 10 Y POS" },
	{ 0x6004 + (10*8), "SPR. 10 MAG" },	
	{ 0x6000 + (11*8), "SPR. 11 X POS" },
	{ 0x6002 + (11*8), "SPR. 11 Y POS" },
	{ 0x6004 + (11*8), "SPR. 11 MAG" },	
	{ 0x6000 + (12*8), "SPR. 12 X POS" },
	{ 0x6002 + (12*8), "SPR. 12 Y POS" },
	{ 0x6004 + (12*8), "SPR. 12 MAG" },	
	{ 0x6000 + (13*8), "SPR. 13 X POS" },
	{ 0x6002 + (13*8), "SPR. 13 Y POS" },
	{ 0x6004 + (13*8), "SPR. 13 MAG" },	
	{ 0x6000 + (14*8), "SPR. 14 X POS" },
	{ 0x6002 + (14*8), "SPR. 14 Y POS" },
	{ 0x6004 + (14*8), "SPR. 14 MAG" },	
	{ 0x6000 + (15*8), "SPR. 15 X POS" },
	{ 0x6002 + (15*8), "SPR. 15 Y POS" },
	{ 0x6004 + (15*8), "SPR. 15 MAG" },

	{ 0x6400 + (0*2), "INK 0" },
	{ 0x6400 + (1*2), "INK 1" },
	{ 0x6400 + (2*2), "INK 2" },
	{ 0x6400 + (3*2), "INK 3" },
	{ 0x6400 + (4*2), "INK 4" },
	{ 0x6400 + (5*2), "INK 5" },
	{ 0x6400 + (6*2), "INK 6" },
	{ 0x6400 + (7*2), "INK 7" },
	{ 0x6400 + (8*2), "INK 8" },
	{ 0x6400 + (9*2), "INK 9" },
	{ 0x6400 + (10*2), "INK 10" },
	{ 0x6400 + (11*2), "INK 11" },
	{ 0x6400 + (12*2), "INK 12" },
	{ 0x6400 + (13*2), "INK 13" },
	{ 0x6400 + (14*2), "INK 14" },
	{ 0x6400 + (15*2), "INK 15" },

	{ 0x6420, "BORDER" },

	{ 0x6420 + (1*2), "SPR. INK 1" },
	{ 0x6420 + (2*2), "SPR. INK 2" },
	{ 0x6420 + (3*2), "SPR. INK 3" },
	{ 0x6420 + (4*2), "SPR. INK 4" },
	{ 0x6420 + (5*2), "SPR. INK 5" },
	{ 0x6420 + (6*2), "SPR. INK 6" },
	{ 0x6420 + (7*2), "SPR. INK 7" },
	{ 0x6420 + (8*2), "SPR. INK 8" },
	{ 0x6420 + (9*2), "SPR. INK 9" },
	{ 0x6420 + (10*2), "SPR. INK 10" },
	{ 0x6420 + (11*2), "SPR. INK 11" },
	{ 0x6420 + (12*2), "SPR. INK 12" },
	{ 0x6420 + (13*2), "SPR. INK 13" },
	{ 0x6420 + (14*2), "SPR. INK 14" },
	{ 0x6420 + (15*2), "SPR. INK 15" },

	{ 0x6800, "PRI Interrupt Line" },
	{ 0x6801, "SPLT Split Line" },
	{ 0x6802, "SSA Secondary Start Adr." },
	{ 0x6804, "SSCR Scroll Control Reg." },
	{ 0x6805, "IVR Interrupt Vector Reg." },

	{ 0x6C00, "SAR0 DMA0 Adr. Ptr" },
	{ 0x6C02, "PPR0 DMA0 Pause Prescaler" },
	{ 0x6C04, "SAR1 DMA1 Adr. Ptr" },
	{ 0x6C06, "PPR1 DMA1 Pause Prescaler" },
	{ 0x6C08, "SAR2 DMA2 Adr. Ptr" },
	{ 0x6C0A, "PPR2 DMA2 Pause Prescaler" },

	{ 0x6C0F, "DCSR DMA Control+Status Reg." },
};

char* GetAsicLabel(unsigned short address)
{
	int nbLabels = sizeof(Labels)/sizeof(struct ASICLabel);
	for (int i=0; i < nbLabels; i++)
	{
		if (Labels[i].adr == address)
		{
			return &Labels[i].label[0];
		}
	}

	return NULL;
}