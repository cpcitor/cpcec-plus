#pragma once

unsigned char cpc_color_table[]=
{
	// 0,    1,    2,  etc.
	0x54, 0x44, 0x55, 0x5C, 0x58, 0x5D, 0x4C, 0x45, 0x4D, 0x56, 0x46, 0x57, 0x5E, 0x40, 0x5F, 0x4E, 0x47, 0x4F, 0x52, 0x42, 0x53, 0x5A, 0x59, 0x5B, 0x4A, 0x43, 0x4B, 0x41, 0x48, 0x49, 0x50, 0x51
};

unsigned char GetBasicColorIndex(unsigned char GAColorIndex)
{
	// 26+"non existing inks till 32"
	for (int i=0; i < 32; i++)
	{
		if (cpc_color_table[i] == GAColorIndex)
		{
			return i;
		}
	}
	return 0; // ??
}

void PrintPalette()
{
	if (plus_enabled)
	{
		int y = WindowHeight-4;
	
		E2;mvprintw(y++, COLUMN_WIDTH, "    PAL0-7 ");D2;
		for (int i=0; i<8; i++)
		{
			E0;printw("%02d", i);D0;E1;printw("%03X ", GetPalColor(i));D1;
		}
		E2;mvprintw(y++, COLUMN_WIDTH, "   PAL8-15 ");D2;
		for (int i=8; i<16; i++)
		{
			E0;printw("%02d", i);D0;E1;printw("%03X ", GetPalColor(i));D1;
		}

		E2;mvprintw(y++, COLUMN_WIDTH, " SPRPAL1-7       ");D2;
		for (int i=1; i<8; i++)
		{
			E0;printw("%02d", i);D0;E1;printw("%03X ", GetPalColor(16+i));D1;
		}
		E2;mvprintw(y++, COLUMN_WIDTH, "SPRPAL8-15 ");D2;
		for (int i=8; i<16; i++)
		{
			E0;printw("%02d", i);D0;E1;printw("%03X ", GetPalColor(16+i));D1;
		}
	}
	else
	{
		int y = WindowHeight-2;
	
		E2;mvprintw(y++, COLUMN_WIDTH, " PAL0-7 ");D2;
		for (int i=0; i<8; i++)
		{
			E0;printw("%02d:", i);D0;E1;printw("%02d ", GetBasicColorIndex(0x40+(gate_table[i] & 31)));D1;
		}
		E2;mvprintw(y++, COLUMN_WIDTH, "PAL8-15 ");D2;
		for (int i=8; i<16; i++)
		{
			E0;printw("%02d:", i);D0;E1;printw("%02d ", GetBasicColorIndex(0x40+(gate_table[i] & 31)));D1;
		}		
	}
}

void PrintASICSprites()
{
	int x = 0;
	int y = TerminalY;
	
	/*for (int spriteIndex=0; spriteIndex<16; spriteIndex++)
	{
		int i = spriteIndex*8;

		int zoomY = (plus_sprite_xyz[i+4]&3);
		int zoomX = (plus_sprite_xyz[i+4]>>2);
		int posY = (plus_sprite_xyz[i+2]+256*plus_sprite_xyz[i+3])&511; // 9-bit wrap!
		int posX = (plus_sprite_xyz[i+0]+256*(signed char)plus_sprite_xyz[i+1]);
		
		int cx = x + (spriteIndex*5);

		E2;mvprintw(y, cx, "");D2;E2;printw("%02d", spriteIndex);D2;
		E2;mvprintw(y+1, cx, "");D2;E1;(posX>=0) ? printw("%03d ", posX):printw("%04d", posX);D1;
		E2;mvprintw(y+2, cx, "");D2;E1;(posY>=0) ? printw("%03d ", posY):printw("%04d", posY);D1;
		E2;mvprintw(y+3, cx, "");D2;E1;printw("%d", zoomX);D1;E0;printw("x");D0;E1;printw("%d", zoomY);D1;
	}*/
	
	for (int spriteIndex=0; spriteIndex<16; spriteIndex++)
	{
		int i = spriteIndex*8;

		int zoomY = (plus_sprite_xyz[i+4]&3);
		int zoomX = (plus_sprite_xyz[i+4]>>2);
		int posY = (plus_sprite_xyz[i+2]+256*plus_sprite_xyz[i+3])&511; // 9-bit wrap!
		int posX = (plus_sprite_xyz[i+0]+256*(signed char)plus_sprite_xyz[i+1]);

		int x = COLUMN_WIDTH;
		if (spriteIndex>=8)
		{
			x += 30;
		}
		int y = TerminalY;
		if (spriteIndex < 8)
		{
			y += spriteIndex;
		}
		else
		{
			y += spriteIndex-8;
		}

		E2;mvprintw(y, x, "");D2;E2;printw("SPR%02d", spriteIndex);D2;
		E1;printw(" %d", zoomX);D1;E0;printw("x");D0;E1;printw("%d", zoomY);D1;
		E0;printw(" X=");D0;E1;printw("%d", posX);D1;
		E0;printw(" Y=");D0;E1;printw("%d", posY);D1;

	}
}

void AskMonitorAddress()
{
	E0;mvprintw(TerminalY, COLUMN_WIDTH, "Address? #");D0;

	MonitorAddress = AskHexadecimalAddress(MonitorIsPreviousValueEntered, EnteredMonitorAddress);

	// MonitorAddress evolves while scrolling in monitor

	MonitorIsPreviousValueEntered = true;

	EnteredMonitorAddress = MonitorAddress;

	MonitorAddressSet = true;
}

void PrintMonitor()
{
	unsigned short address = MonitorAddress;

	while (TerminalY < WindowHeight)
	{
		E0;mvprintw(TerminalY, COLUMN_WIDTH, "#%04X", address);

		for (int i=0; i<16; i++)
		{
			BYTE b = z80_debug_peek(z80_debug_peekpoke, address + i);

			//if ((i&1)==0)
			{
				E1;printw(" %02X", b);D1;
			}
			//else
			{
				//E0;printw(" %02X", b);D0;
			}
		}
		printw(" ");
		E2;
		for (int i=0; i<16; i++)
		{
			BYTE b = z80_debug_peek(z80_debug_peekpoke, address + i);

			if ((b >= 32) && (b<127))
			{
				printw("%c", b);
			}
			else
			{
				printw(".", b);
			}
		}
		D2;

		address += 16;

		TerminalY++;
	}
}

void ExportMemoryFragment()
{
	mvprintw(WindowHeight-1, COLUMN_WIDTH, "                                                 ");
	E0;mvprintw(WindowHeight-1, COLUMN_WIDTH, "Start Address? #");D0;
	unsigned short Start = AskHexadecimalAddress(false, 0);

	mvprintw(WindowHeight-1, COLUMN_WIDTH, "                                                 ");
	E0;mvprintw(WindowHeight-1, COLUMN_WIDTH, "Length? #");D0;
	unsigned short Length = AskHexadecimalAddress(false, 0);

	mvprintw(WindowHeight-1, COLUMN_WIDTH, "                                                 ");
	E0;mvprintw(WindowHeight-1, COLUMN_WIDTH, "Filename? ");D0;
	char str[64];
	AskString(str, sizeof(str));

	char filename[128];
	//strcpy(filename, "~/");
	//strcat(filename, str);
	strcpy(filename, str);

	mvprintw(WindowHeight-1, COLUMN_WIDTH, "                                                 ");
	FILE* file = fopen(filename, "wb");
	if (file == NULL)
	{
		E0;mvprintw(WindowHeight-1, COLUMN_WIDTH, "Error writing the file");D0;
		AskString(str, 0);
		return;
	}
	for(int i=0; i<Length; i++)
	{
		BYTE b = z80_debug_peek(z80_debug_peekpoke, Start + i);
		// let's not fwrite as a single block but byte per byte,
		// since memory configuration (ROM/RAM) may change+cycle back to 0
		fwrite(&b, 1, 1, file);
	}
	fclose(file);

	E0;mvprintw(WindowHeight-1, COLUMN_WIDTH, "File written");D0;
	AskString(str, 0);

	ForceNextRefresh = true;
}