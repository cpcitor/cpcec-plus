
#pragma once

// Draw sprites into the video buffer if required
// Returns true if rendering has been processed (cf. if Sprites mode is active in Terminal)
bool DrawSDLSpriteViewer()
{
	if (!ShowSpriteViewer)
	{
		return false;
	}

	for (int y=0;y<VIDEO_PIXELS_Y;++y)
	{
		memset(&debug_frame[y*VIDEO_PIXELS_X], 0x00202020, VIDEO_PIXELS_X * sizeof(VIDEO_UNIT));
		/*for (int x=0;x<VIDEO_PIXELS_X;++x)
		{
			debug_frame[(y*VIDEO_PIXELS_X)+x] = 0x00202020; // 0x00RRGGBB
		}*/
	}

#define ZOOM 7
#define SPR_WIDTH 16
#define SPR_HEIGHT 16
#define NB_PIXELS_BETWEEN_SPRITES 2

	int sdlPosX = NB_PIXELS_BETWEEN_SPRITES;
	int sdlPosY = NB_PIXELS_BETWEEN_SPRITES;
	for (int spriteIndex=0; spriteIndex<16; )
	{
		BYTE* SpriteData = &plus_sprite_bmp[spriteIndex * 256];

		for (int sprY=0; sprY<16; sprY++)
		{
			for (int sprX=0; sprX<16; sprX++)
			{
				BYTE asicPixel = *(SpriteData++);

				for (int zoomY=0; zoomY<ZOOM; zoomY++)
				{
					for (int zoomX=0; zoomX<ZOOM; zoomX++)
					{
						debug_frame[ ((((sdlPosY + sprY)*ZOOM)+zoomY) * VIDEO_PIXELS_X)
									+ ((sdlPosX + sprX)*ZOOM)+zoomX] = video_clut[16+asicPixel];
					}
				}
			}
		}

		sdlPosX += SPR_WIDTH + NB_PIXELS_BETWEEN_SPRITES;

		spriteIndex++;
		if ((spriteIndex & 3)==0)
		{
			sdlPosX = NB_PIXELS_BETWEEN_SPRITES;
			sdlPosY += SPR_HEIGHT + NB_PIXELS_BETWEEN_SPRITES;
		}
	}

	return true;
}
